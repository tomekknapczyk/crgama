# CR GAMA

#### A search engine that allows you to find an authorized installation service of phase voltage regulators

### First run:
```bash
#setup docker environment
$ docker-compose up -d

# access web container
$ docker exec -it crgama_web_1 bash

# install dependencies
root@:/var/www/crgama# bin/console composer install 

#migrate database
root@:/var/www/crgama# bin/console d:m:m 
```

### Build assets

```bash
$ npm install

# watch live changes of styles
$ npm run watch

# build production files
$ npm run prod

# tailwind build process with purge unused classes 
$ npx tailwindcss -i assets/styles/app.css -o public/build/app.css --watch
```