<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220801182959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Relations change';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE service_zip_code (service_id INT NOT NULL, zip_code_id INT NOT NULL, INDEX IDX_58DABA65ED5CA9E6 (service_id), INDEX IDX_58DABA659CEB97F7 (zip_code_id), PRIMARY KEY(service_id, zip_code_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE service_zip_code ADD CONSTRAINT FK_58DABA65ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE service_zip_code ADD CONSTRAINT FK_58DABA659CEB97F7 FOREIGN KEY (zip_code_id) REFERENCES zip_code (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE query_electrician');
        $this->addSql('ALTER TABLE service ADD code_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD227DAFE17 FOREIGN KEY (code_id) REFERENCES zip_code (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD227DAFE17 ON service (code_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE query_electrician (query_id INT NOT NULL, electrician_id INT NOT NULL, INDEX IDX_56AAEB11A916AF23 (electrician_id), INDEX IDX_56AAEB11EF946F99 (query_id), PRIMARY KEY(query_id, electrician_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE query_electrician ADD CONSTRAINT FK_56AAEB11A916AF23 FOREIGN KEY (electrician_id) REFERENCES electrician (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE query_electrician ADD CONSTRAINT FK_56AAEB11EF946F99 FOREIGN KEY (query_id) REFERENCES query (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE service_zip_code');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD227DAFE17');
        $this->addSql('DROP INDEX IDX_E19D9AD227DAFE17 ON service');
        $this->addSql('ALTER TABLE service DROP code_id');
    }
}
