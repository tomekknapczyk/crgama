<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220802182058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'query created time';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE query ADD created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE query DROP created_at');
    }
}
