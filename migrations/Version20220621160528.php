<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220621160528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Service workers';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE electrician ADD service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE electrician ADD CONSTRAINT FK_BBD9FFEEED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('CREATE INDEX IDX_BBD9FFEEED5CA9E6 ON electrician (service_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE electrician DROP FOREIGN KEY FK_BBD9FFEEED5CA9E6');
        $this->addSql('DROP INDEX IDX_BBD9FFEEED5CA9E6 ON electrician');
        $this->addSql('ALTER TABLE electrician DROP service_id');
    }
}
