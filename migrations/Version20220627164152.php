<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220627164152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Query entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE query (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, street VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, delivery_street VARCHAR(255) DEFAULT NULL, delivery_city VARCHAR(255) DEFAULT NULL, code VARCHAR(6) NOT NULL, email_consent TINYINT(1) NOT NULL, phone_consent TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE query');
    }
}
