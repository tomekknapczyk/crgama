<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220629182626 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'File attachment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE electrician ADD file_name VARCHAR(255) DEFAULT NULL, ADD file_size INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD file_name VARCHAR(255) DEFAULT NULL, ADD file_size INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE electrician DROP file_name, DROP file_size');
        $this->addSql('ALTER TABLE service DROP file_name, DROP file_size');
    }
}
