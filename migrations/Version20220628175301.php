<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220628175301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Query relations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE query_electrician (query_id INT NOT NULL, electrician_id INT NOT NULL, INDEX IDX_56AAEB11EF946F99 (query_id), INDEX IDX_56AAEB11A916AF23 (electrician_id), PRIMARY KEY(query_id, electrician_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE query_service (query_id INT NOT NULL, service_id INT NOT NULL, INDEX IDX_AB37ABF0EF946F99 (query_id), INDEX IDX_AB37ABF0ED5CA9E6 (service_id), PRIMARY KEY(query_id, service_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE query_electrician ADD CONSTRAINT FK_56AAEB11EF946F99 FOREIGN KEY (query_id) REFERENCES query (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE query_electrician ADD CONSTRAINT FK_56AAEB11A916AF23 FOREIGN KEY (electrician_id) REFERENCES electrician (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE query_service ADD CONSTRAINT FK_AB37ABF0EF946F99 FOREIGN KEY (query_id) REFERENCES query (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE query_service ADD CONSTRAINT FK_AB37ABF0ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE query ADD zip_code_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE query ADD CONSTRAINT FK_24BDB5EB9CEB97F7 FOREIGN KEY (zip_code_id) REFERENCES zip_code (id)');
        $this->addSql('CREATE INDEX IDX_24BDB5EB9CEB97F7 ON query (zip_code_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE query_electrician');
        $this->addSql('DROP TABLE query_service');
        $this->addSql('ALTER TABLE query DROP FOREIGN KEY FK_24BDB5EB9CEB97F7');
        $this->addSql('DROP INDEX IDX_24BDB5EB9CEB97F7 ON query');
        $this->addSql('ALTER TABLE query DROP zip_code_id');
    }
}
