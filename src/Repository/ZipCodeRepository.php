<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ZipCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ZipCode>
 *
 * @method ZipCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZipCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZipCode[]    findAll()
 * @method ZipCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZipCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZipCode::class);
    }

    public function save(ZipCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ZipCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
