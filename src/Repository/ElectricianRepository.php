<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Electrician;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Electrician>
 *
 * @method Electrician|null find($id, $lockMode = null, $lockVersion = null)
 * @method Electrician|null findOneBy(array $criteria, array $orderBy = null)
 * @method Electrician[]    findAll()
 * @method Electrician[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElectricianRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Electrician::class);
    }

    public function save(Electrician $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Electrician $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
