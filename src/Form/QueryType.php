<?php

namespace App\Form;

use App\Entity\Query;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $formFieldClass = 'w-full rounded-2xl bg-neutral-100 p-3 mb-3 transition';
        $formBtnClass = 'w-1/2 mx-auto block mt-6 rounded-full text-white bg-blue-500 p-3 mb-3 hover:bg-blue-700 transition uppercase';

        $builder
            ->add('name', TextType::class,[
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Imię *'
                ],
                'label' => false,
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Nazwisko *'
                ],
                'label' => false,
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'E-mail *'
                ],
                'label' => false,
            ])
            ->add('phone', TelType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Telefon *'
                ],
                'label' => false,
            ])
            ->add('street', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Ulica'
                ],
                'label' => false,
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Miasto'
                ],
                'label' => false,
                'required' => false,
            ])
            ->add('deliveryStreet', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Ulica'
                ],
                'label' => false,
                'required' => false,
            ])
            ->add('deliveryCity', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Miasto'
                ],
                'label' => false,
                'required' => false,
            ])
            ->add('code', TextType::class, [
                'attr' => [
                    'class' => $formFieldClass,
                    'placeholder' => 'Kod pocztowy *'
                ],
                'label' => false,
                'help' => 'Kod pocztowy w formacie xx-yyy',
                'help_attr' => [
                    'class' => 'text-xs -mt-2 ml-2 text-gray-400',
                ],
            ])
            ->add('emailConsent', CheckboxType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('phoneConsent', CheckboxType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'szukaj',
                'attr' => [
                    'class' => $formBtnClass,
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Query::class,
        ]);
    }
}