<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Setting;
use JetBrains\PhpStorm\Pure;

class SettingService
{
    #[Pure] public function getRealValue(Setting $setting): bool|int|string|null
    {
        return match ($setting->getType()) {
            'integer' => (int)$setting->getValue(),
            'boolean' => (bool)$setting->getValue(),
            default => $setting->getValue(),
        };
    }
}
