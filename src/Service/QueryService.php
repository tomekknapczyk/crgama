<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Query;
use App\Entity\ZipCode;
use App\Repository\QueryRepository;
use App\Repository\ZipCodeRepository;
use Symfony\Component\Form\FormInterface;

class QueryService
{
    private QueryRepository $queryRepository;
    private ZipCodeRepository $zipCodeRepository;

    public function __construct(
        QueryRepository $queryRepository,
        ZipCodeRepository $zipCodeRepository
    ) {
        $this->queryRepository = $queryRepository;
        $this->zipCodeRepository = $zipCodeRepository;
    }

    public function createQueryFromForm(Query $query, FormInterface $form): array
    {
        $services = [];
        $zipCode = $this->zipCodeRepository->findOneBy(['code' => $form['code']->getData()]);

        if ($zipCode instanceof ZipCode) {
            $services = $zipCode->getServices()->toArray();

            foreach ($services as $service) {
                $query->addService($service);
            }

            $query->setZipCode($zipCode);
        }

        $this->queryRepository->save($query, true);

        return $services;
    }
}