<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\QueryRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: QueryRepository::class)]
class Query
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    private string $email;

    #[ORM\Column(type: 'string', length: 255)]
    private string $phone;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $street = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $deliveryStreet = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $deliveryCity = null;

    #[ORM\Column(type: 'string', length: 6)]
    #[Assert\Regex(
        pattern: '/(^\d{2}-\d{3}$)/m',
        message: 'Nieprawidłowy kod pocztowy. Wpisz kod w formacie xx-yyy.'
    )]
    private string $code;

    #[ORM\Column(type: 'boolean')]
    private bool $emailConsent = false;

    #[ORM\Column(type: 'boolean')]
    private bool $phoneConsent = false;

    #[ORM\ManyToMany(targetEntity: Service::class, inversedBy: 'queries')]
    private Collection $services;

    #[ORM\ManyToOne(targetEntity: ZipCode::class, inversedBy: 'queries')]
    private ?ZipCode $zipCode = null;

    #[ORM\Column(type: 'datetime_immutable')]
    #[ORM\JoinColumn(nullable: true)]
    private ?DateTimeImmutable $created_at;

    #[Pure] public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
        $this->services = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDeliveryStreet(): ?string
    {
        return $this->deliveryStreet;
    }

    public function setDeliveryStreet(?string $deliveryStreet): self
    {
        $this->deliveryStreet = $deliveryStreet;

        return $this;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?string $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function isEmailConsent(): bool
    {
        return $this->emailConsent;
    }

    public function setEmailConsent(bool $emailConsent): self
    {
        $this->emailConsent = $emailConsent;

        return $this;
    }

    public function isPhoneConsent(): bool
    {
        return $this->phoneConsent;
    }

    public function setPhoneConsent(bool $phoneConsent): self
    {
        $this->phoneConsent = $phoneConsent;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name.' '.$this->lastname;
    }

    /**
     * @return Collection<int, Service>
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        $this->services->removeElement($service);

        return $this;
    }

    public function getZipCode(): ?ZipCode
    {
        return $this->zipCode;
    }

    public function setZipCode(?ZipCode $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
