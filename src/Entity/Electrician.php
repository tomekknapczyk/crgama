<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Trait\FileAttachmentTrait;
use App\Repository\ElectricianRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;

#[ORM\Entity(repositoryClass: ElectricianRepository::class)]
#[Uploadable]
class Electrician
{
    use FileAttachmentTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    private string $phone;

    #[ORM\Column(type: 'string', length: 255)]
    private string $sepNumber;

    #[ORM\ManyToMany(targetEntity: ZipCode::class, inversedBy: 'electricians')]
    private Collection $codes;

    #[ORM\ManyToOne(targetEntity: Service::class, inversedBy: 'electricians')]
    private ?Service $service;

    #[Pure] public function __construct()
    {
        $this->codes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getSepNumber(): string
    {
        return $this->sepNumber;
    }

    public function setSepNumber(string $sepNumber): self
    {
        $this->sepNumber = $sepNumber;

        return $this;
    }

    /**
     * @return Collection<int, ZipCode>
     */
    public function getCodes(): Collection
    {
        return $this->codes;
    }

    public function addCode(ZipCode $code): self
    {
        if (!$this->codes->contains($code)) {
            $this->codes[] = $code;
        }

        return $this;
    }

    public function removeCode(ZipCode $code): self
    {
        $this->codes->removeElement($code);

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name.' '.$this->lastname;
    }
}
