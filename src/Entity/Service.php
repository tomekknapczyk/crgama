<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Trait\FileAttachmentTrait;
use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;

#[ORM\Entity(repositoryClass: ServiceRepository::class)]
#[Uploadable]
class Service
{
    use FileAttachmentTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private string $street;

    #[ORM\Column(type: 'string', length: 255)]
    private string $city;

    #[ORM\Column(type: 'string', length: 255)]
    private string $county;

    #[ORM\Column(type: 'string', length: 255)]
    private string $nip;

    #[ORM\Column(type: 'string', length: 255)]
    private string $contractNumber;

    #[ORM\Column(type: 'string', length: 255)]
    private string $email;

    #[ORM\Column(type: 'string', length: 255)]
    private string $phone;

    #[ORM\OneToMany(mappedBy: 'service', targetEntity: Electrician::class)]
    private Collection $electricians;

    #[ORM\ManyToMany(targetEntity: Query::class, mappedBy: 'services')]
    private Collection $queries;

    #[ORM\ManyToMany(targetEntity: ZipCode::class, inversedBy: 'services')]
    private Collection $codes;

    #[ORM\ManyToOne(targetEntity: ZipCode::class)]
    private ZipCode $code;

    #[Pure] public function __construct()
    {
        $this->electricians = new ArrayCollection();
        $this->queries = new ArrayCollection();
        $this->codes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCounty(): string
    {
        return $this->county;
    }

    public function setCounty(string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getNip(): string
    {
        return $this->nip;
    }

    public function setNip(string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getContractNumber(): string
    {
        return $this->contractNumber;
    }

    public function setContractNumber(string $contractNumber): self
    {
        $this->contractNumber = $contractNumber;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection<int, Electrician>
     */
    public function getElectricians(): Collection
    {
        return $this->electricians;
    }

    public function addElectrician(Electrician $electrician): self
    {
        if (!$this->electricians->contains($electrician)) {
            $this->electricians[] = $electrician;
            $electrician->setService($this);
        }

        return $this;
    }

    public function removeElectrician(Electrician $electrician): self
    {
        if ($this->electricians->removeElement($electrician)) {
            if ($electrician->getService() === $this) {
                $electrician->setService(null);
            }
        }

        return $this;
    }

    public function getCode(): ZipCode
    {
        return $this->code;
    }

    public function setCode(ZipCode $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Query>
     */
    public function getQueries(): Collection
    {
        return $this->queries;
    }

    public function addQuery(Query $query): self
    {
        if (!$this->queries->contains($query)) {
            $this->queries[] = $query;
            $query->addService($this);
        }

        return $this;
    }

    public function removeQuery(Query $query): self
    {
        if ($this->queries->removeElement($query)) {
            $query->removeService($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, ZipCode>
     */
    public function getCodes(): Collection
    {
        return $this->codes;
    }

    public function addCode(ZipCode $code): self
    {
        if (!$this->codes->contains($code)) {
            $this->codes[] = $code;
        }

        return $this;
    }

    public function removeCode(ZipCode $code): self
    {
        $this->codes->removeElement($code);

        return $this;
    }

    public function getAddress(): string
    {
        return $this->street.", ".$this->city.", ".$this->county;
    }
}
