<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ZipCodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ZipCodeRepository::class)]
class ZipCode
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 6)]
    #[Assert\Regex(
        pattern: '/(^\d{2}-\d{3}$)/m',
        message: 'Nieprawidłowy kod pocztowy. Wpisz kod w formacie xx-yyy.'
    )]
    private string $code;

    #[ORM\ManyToMany(targetEntity: Service::class, mappedBy: 'codes')]
    private Collection $services;

    #[ORM\OneToMany(mappedBy: 'zipCode', targetEntity: Query::class)]
    private Collection $queries;

    #[Pure] public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->queries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection<int, Service>
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addCode($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->removeElement($service)) {
            $service->removeCode($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->code;
    }

    /**
     * @return Collection<int, Query>
     */
    public function getQueries(): Collection
    {
        return $this->queries;
    }

    public function addQuery(Query $query): self
    {
        if (!$this->queries->contains($query)) {
            $this->queries[] = $query;
            $query->setZipCode($this);
        }

        return $this;
    }

    public function removeQuery(Query $query): self
    {
        if ($this->queries->removeElement($query)) {
            if ($query->getZipCode() === $this) {
                $query->setZipCode(null);
            }
        }

        return $this;
    }
}
