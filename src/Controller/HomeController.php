<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Query;
use App\Entity\Setting;
use App\Form\QueryType;
use App\Repository\SettingRepository;
use App\Service\QueryService;
use App\Service\SettingService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route(path: '/', name: 'home')]
    public function home(Request $request, QueryService $queryService, SettingRepository $settingRepository, SettingService $settingService): Response
    {
        $query = new Query();
        $results = [];

        $form = $this->createForm(QueryType::class, $query);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $results = $queryService->createQueryFromForm($query, $form);
                    if (!count($results)) {
                        $results = null;
                    }
                } catch (Exception $e) {
                    $this->addFlash('warning', $e->getMessage());
                }
            }
        } catch (Exception) {
            $this->addFlash('warning', 'Wystąpił błąd podczas próby wyszukiwania. Spróbuj jeszcze raz.');
        }

        $moreInfoUrlSetting = $settingRepository->findOneBy(['slug' => 'url-dowiedz-sie-wiecej']);
        $moreInfoUrl = $moreInfoUrlSetting instanceof Setting ? $settingService->getRealValue($moreInfoUrlSetting) : null;

        return $this->renderForm('home.html.twig', [
            'form' => $form,
            'results' => $results,
            'moreInfoUrl' => $moreInfoUrl,
        ]);
    }
}
