<?php

namespace App\Controller\Admin;

use App\Entity\Electrician;
use App\Entity\Query;
use App\Entity\Service;
use App\Entity\Setting;
use App\Entity\ZipCode;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(QueryCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Crgama');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Serwisy', 'fas fa-bolt', Service::class);
        yield MenuItem::linkToCrud('Elektrycy', 'fas fa-users', Electrician::class);
        yield MenuItem::linkToCrud('Kody pocztowe', 'fa-solid fa-map-location', ZipCode::class);
        yield MenuItem::linkToCrud('Wyszukiwania', 'fa-solid fa-map-location', Query::class);
        yield MenuItem::linkToCrud('Ustawienia', 'fa-solid fa-cogs', Setting::class);
    }
}
