<?php

namespace App\Controller\Admin;

use App\Entity\Query;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class QueryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Query::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Wyszukiwania')
            ->setEntityLabelInPlural('Wyszukiwania')
            ->showEntityActionsInlined()
            ->setDefaultSort(['created_at' => 'DESC'])
            ->setPaginatorPageSize(25);
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Imię');
        yield TextField::new('lastname', 'Nazwisko');
        yield EmailField::new('email', 'Email');
        yield TelephoneField::new('phone', 'Telefon');
        yield TextField::new('street', 'Ulica');
        yield TextField::new('city', 'Miasto');
        yield TextField::new('deliveryStreet', 'Montaż ulica');
        yield TextField::new('deliveryCity', 'Montaż miasto');
        yield TextField::new('code', 'Kod pocztowy');
        yield TelephoneField::new('created_at', 'Data utworzenia')->setTemplatePath('admin/created.html.twig');
        yield BooleanField::new('emailConsent', 'Zgoda na kontakt mailowy')->onlyOnDetail();
        yield BooleanField::new('phoneConsent', 'Zgoda na kontakt telefoniczny')->onlyOnDetail();
        yield AssociationField::new('services', 'Wyświetlone serwisy')->onlyOnDetail()->setTemplatePath('admin/service.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Crud::PAGE_NEW)
            ->remove(Crud::PAGE_INDEX, Crud::PAGE_EDIT)
            ->remove(Crud::PAGE_DETAIL, Crud::PAGE_EDIT)
            ->add(Crud::PAGE_INDEX, Crud::PAGE_DETAIL);
    }
}
