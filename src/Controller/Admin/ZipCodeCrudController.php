<?php

namespace App\Controller\Admin;

use App\Entity\ZipCode;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ZipCodeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ZipCode::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Kod pocztowy')
            ->setEntityLabelInPlural('Kody pocztowe')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('code', 'Kod pocztowy')->setHelp('Kod pocztowy w formacie xx-yyy');
        yield AssociationField::new('services', 'Obsługujące serwisy')->onlyOnIndex();
        yield AssociationField::new('services', 'Obsługujące serwisy')->onlyOnDetail()->setTemplatePath('admin/service.html.twig');
        yield AssociationField::new('queries', 'Wyszukiwania')->onlyOnIndex();
        yield AssociationField::new('queries', 'Wyszukiwania')->onlyOnDetail()->setTemplatePath('admin/query.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}
