<?php

namespace App\Controller\Admin;

use App\Entity\Setting;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SettingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Setting::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Ustawienie')
            ->setEntityLabelInPlural('Ustawienia')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Nazwa ustawienia');
        yield SlugField::new('slug', 'Alias')->setTargetFieldName('name')->setUnlockConfirmationMessage(
            'Wysoce zalecane jest używanie automatycznych aliasów, ale zawsze możesz je dostosować'
        );
        yield ChoiceField::new('type', 'Typ pola')->setChoices([
            'tekst' => 'string',
            'liczba' => 'integer',
        ]);
        yield TextField::new('value', 'Wartość');
    }
}
