<?php

namespace App\Controller\Admin;

use App\Entity\Electrician;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ElectricianCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Electrician::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Elektryk')
            ->setEntityLabelInPlural('Elektrycy')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Imię');
        yield TextField::new('lastname', 'Nazwisko');
        yield TelephoneField::new('phone', 'Numer telefonu');
        yield TextField::new('sepNumber', 'Numer uprawnień SEP');
        yield AssociationField::new('service', 'Serwis');
        yield AssociationField::new('codes', 'Obsługiwane kody pocztowe')->onlyOnForms()->autocomplete();
        yield AssociationField::new('codes', 'Obsługiwane kody pocztowe')->onlyOnDetail()->setTemplatePath('admin/zip_code.html.twig');

        yield ImageField::new('fileName', 'Plik')->onlyOnForms()->setUploadDir('public/files/');
        yield ImageField::new('fileName', 'Plik')->onlyOnDetail()->setTemplatePath('admin/file.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}
