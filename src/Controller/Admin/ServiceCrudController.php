<?php

namespace App\Controller\Admin;

use App\Entity\Service;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ServiceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Service::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Serwis')
            ->setEntityLabelInPlural('Serwisy')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Nazwa firmy');
        yield TextField::new('address', 'Adres')->onlyOnIndex();
        yield TextField::new('street', 'Ulica')->hideOnIndex();
        yield TextField::new('city', 'Miasto')->hideOnIndex();
        yield TextField::new('county', 'Powiat')->hideOnIndex();
        yield TextField::new('nip', 'NIP');
        yield TextField::new('contractNumber', 'Numer umowy')->hideOnIndex();
        yield EmailField::new('email', 'Email');
        yield TelephoneField::new('phone', 'Numer telefonu');
        yield AssociationField::new('code', 'Główny kod')->setHelp('Kod pocztowy w formacie xx-yyy');
        yield AssociationField::new('codes', 'Obsługiwane kody pocztowe')->onlyOnForms();
        yield AssociationField::new('codes', 'Obsługiwane kody pocztowe')->onlyOnDetail()->setTemplatePath('admin/zip_code.html.twig');
        yield AssociationField::new('electricians', 'Elektrycy')->onlyOnIndex();
        yield AssociationField::new('electricians', 'Elektrycy')->onlyOnDetail()->setTemplatePath('admin/electrician.html.twig');
        yield AssociationField::new('queries', 'Wyszukiwania')->onlyOnIndex();
        yield AssociationField::new('queries', 'Wyszukiwania')->onlyOnDetail()->setTemplatePath('admin/query.html.twig');

        yield ImageField::new('fileName', 'Plik')->onlyOnForms()->setUploadDir('public/files/');
        yield ImageField::new('fileName', 'Plik')->onlyOnDetail()->setTemplatePath('admin/file.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}
